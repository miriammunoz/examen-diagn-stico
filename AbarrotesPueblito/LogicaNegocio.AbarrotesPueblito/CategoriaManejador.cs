﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.AbarrotesPueblito;
using AccesoDatos.AbarrotesPueblito;

namespace LogicaNegocio.AbarrotesPueblito
{
    public class CategoriaManejador
    {
        private CategoriaAccesoDatos _categoriaAccesoDatos = new CategoriaAccesoDatos();

        public void Guardar(Categoria categoria)
        {
            _categoriaAccesoDatos.Guardar(categoria);

        }
        public void Eliminar(int idCategoria)
        {
            _categoriaAccesoDatos.Eliminar(idCategoria);
        }
        public List<Categoria> GetCategoria(string filtro)
        {
            var listCategoria = _categoriaAccesoDatos.GetCategoria(filtro);

            return listCategoria;
        }
    }
}
