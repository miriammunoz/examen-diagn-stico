﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.AbarrotesPueblito
{
    public class Categoria
    {
        private int _idcategoria;
        private string _nombrec;

        public int Idcategoria { get => _idcategoria; set => _idcategoria = value; }
        public string Nombrec { get => _nombrec; set => _nombrec = value; }
    }
}
