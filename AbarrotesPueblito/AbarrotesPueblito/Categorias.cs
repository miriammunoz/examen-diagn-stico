﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.AbarrotesPueblito;
using LogicaNegocio.AbarrotesPueblito;

namespace AbarrotesPueblito
{
    public partial class Categorias : Form
    {
        private CategoriaManejador _categoriaManejador;
        public Categorias()
        {
            InitializeComponent();
            _categoriaManejador = new CategoriaManejador();
        }

        private void Categoria_Load(object sender, EventArgs e)
        {
            buscarcategoria("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscarcategoria(string filtro)
        {
            dtgCategoria.DataSource = _categoriaManejador.GetCategoria(filtro);
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtncategoria.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            txtncategoria.Text = "";
            

        }
        private void guardarCategoria()
        {
            _categoriaManejador.Guardar(new Categoria
            {
                Idcategoria = Convert.ToInt32(lblId.Text),
                Nombrec = txtncategoria.Text.ToString()
            });
        }
        private void EliminarCategoria()
        {
            var Idcategoria = dtgCategoria.CurrentRow.Cells["idcategoria"].Value;
            _categoriaManejador.Eliminar(Convert.ToInt32(Idcategoria));
        }
        private void ModificarCategoria()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgCategoria.CurrentRow.Cells["idcategoria"].Value.ToString();
            txtncategoria.Text = dtgCategoria.CurrentRow.Cells["nombrec"].Value.ToString();

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarcategoria("");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtncategoria.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                guardarCategoria();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarCategoria();
                    buscarcategoria("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void dtgCategoria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarCategoria();
                buscarcategoria("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtncategoria_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
