﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.AbarrotesPueblito;
using System.Data;

namespace AccesoDatos.AbarrotesPueblito
{
    public class CategoriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CategoriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3309);
        }
        public void Guardar(Categoria categoria)
        {
            if (categoria.Idcategoria == 0)
            {
                string consulta = string.Format("Insert into categoria values(null,'{1}')",
                categoria.Idcategoria, categoria.Nombrec);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update categoria set nombrec ='{0}' where idcategoria = '{1}' ",
                categoria.Nombrec, categoria.Idcategoria);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idCategoria)
        {
            string consulta = string.Format("Delete from categoria where idcategoria = {0}", idCategoria);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Categoria> GetCategoria(string filtro)
        {
            var listCategoria = new List<Categoria>();
            var ds = new DataSet();
            string consulta = "select * from categoria where Nombrec like '%"+filtro+"%'";
            ds = conexion.ObtenerDatos(consulta, "categoria");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var categoria = new Categoria
                {
                    Idcategoria = Convert.ToInt32(row["idcategoria"]),
                    Nombrec = row["Nombrec"].ToString()
                    
                };
                listCategoria.Add(categoria);
            }
            return listCategoria;
        }
    }
}
